import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public formGroupLogin: FormGroup;
  
  constructor(public formBuilder: FormBuilder) {
    this.createFormLogin(formBuilder);
  }

  createFormLogin(formBuilder: FormBuilder) {
    this.formGroupLogin = this.formBuilder.group({
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.pattern('[a-zA-Z]{1,10}')
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.pattern('[a-zA-Z0-9]{1,10}')
      ])]
    });
  }

  getUsername() {
    return this.formGroupLogin.get('username');
  }

  getPassword() {
    return this.formGroupLogin.get('password');
  }

  goLogin() {
    if (this.formGroupLogin.valid) {

    }
  }

  onChangeFormGroupLogin() {
    this.formGroupLogin.valueChanges.subscribe((value) => {
    
  });
  }

  onChangeFormControlUsername() {
    this.formGroupLogin.get('username').valueChanges.subscribe((value) => {

    });
  }

  onChangeFormControlPassword() {
    this.formGroupLogin.get('password').valueChanges.subscribe((value) => {

    });
  }

}
